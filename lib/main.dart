import 'package:app_cantor/src/app.dart';
import 'package:app_cantor/src/models/cantor_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => CantorModel()),
      ],
      child: App(),
    ),
  );
}
