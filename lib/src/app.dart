import 'package:app_cantor/src/views/cantor/cantor_page.dart';
import 'package:app_cantor/src/views/home/home_page.dart';
import 'package:flutter/material.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'App do Cantor',
      theme: ThemeData(
          primaryColor: Color.fromRGBO(0, 150, 136, 1),
          accentColor: Color.fromRGBO(178, 223, 219, 1),
          hoverColor: Color.fromRGBO(158, 158, 158, 1),
          scaffoldBackgroundColor: Color.fromRGBO(250, 250, 250, 1),
          fontFamily: 'Montserrat'),
      initialRoute: '/',
      routes: {
        '/': (context) => HomePage(),
        '/sobre': (context) => CantorPage(),
      },
    );
  }
}
