import 'package:flutter/material.dart';
import 'DrawerListTile.dart';

class HomeDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            decoration: BoxDecoration(
              color: Color.fromRGBO(0, 121, 107, 1),
            ),
            child: Text(
              'Seja bem-vindo!',
              style: TextStyle(
                color: Colors.white,
                fontSize: 24,
              ),
            ),
          ),
          DrawerListTile(
            icon: Icons.account_circle,
            title: 'Sobre',
            rota: '/sobre',
          ),
          DrawerListTile(
            icon: Icons.date_range,
            title: 'Agenda de Shows',
            rota: '/',
          ),
          DrawerListTile(
            icon: Icons.library_music,
            title: 'Álbuns',
            rota: '/',
          ),
        ],
      ),
    );
  }
}
