import 'package:app_cantor/src/models/cantor_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomeFloatingActionButton extends StatelessWidget {
  const HomeFloatingActionButton({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<CantorModel>(builder: (context, cantor, child) {
      return FloatingActionButton(
        onPressed: () {
          cantor.enviarMensagemWhatsapp();
        },
        backgroundColor: Color.fromRGBO(70, 198, 85, 1),
        child: Image.asset(
          'assets/images/logo_whatsapp.png',
        ),
      );
    });
  }
}
