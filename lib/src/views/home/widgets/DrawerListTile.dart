import 'package:app_cantor/src/theme.dart';
import 'package:flutter/material.dart';

class DrawerListTile extends StatelessWidget {
  const DrawerListTile({
    Key? key,
    required this.icon,
    required this.title,
    required this.rota,
  }) : super(key: key);

  final IconData icon;
  final String title;
  final String rota;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Icon(
        icon,
        color: iconColor,
      ),
      title: Text(title),
      onTap: () {
        Navigator.pushNamed(context, rota);
      },
    );
  }
}
