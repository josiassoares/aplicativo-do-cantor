import 'package:app_cantor/src/models/cantor_model.dart';
import 'package:app_cantor/src/views/home/widgets/HomeDrawer.dart';
import 'package:app_cantor/src/views/home/widgets/HomeFloatingActionButton.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<CantorModel>(
      builder: (context, cantor, child) {
        return Scaffold(
          appBar: AppBar(
            title: Text(
              'MARCELA ROCHA',
              style: TextStyle(color: Colors.white),
            ),
            centerTitle: true,
          ),
          drawer: HomeDrawer(),
          floatingActionButton: HomeFloatingActionButton(),
        );
      },
    );
  }
}
