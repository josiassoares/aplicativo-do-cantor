import 'package:app_cantor/src/models/cantor_model.dart';
import 'package:app_cantor/src/views/cantor/widgets/CantorContatoCard.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CantorPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<CantorModel>(
      builder: (context, cantor, child) {
        return Scaffold(
          appBar: AppBar(
            title: Text(
              'Sobre',
              style: TextStyle(color: Colors.white),
            ),
            centerTitle: true,
          ),
          body: ListView(
            padding: EdgeInsets.symmetric(
                vertical: 35.0,
                horizontal: MediaQuery.of(context).size.width / 12),
            children: [
              Text(
                cantor.nome!,
                style: TextStyle(
                  fontSize: 35.0,
                  color: Colors.black87,
                  fontWeight: FontWeight.w500,
                ),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 25.0,
              ),
              CircleAvatar(
                radius: 140.0,
                backgroundImage: AssetImage(cantor.urlImagemPerfil!),
              ),
              SizedBox(
                height: 25.0,
              ),
              CantorContatoCard(
                text: cantor.telefone!,
                icon: Icons.phone,
                onTap: () {
                  cantor.fazerLigacao();
                },
              ),
              CantorContatoCard(
                text: cantor.email!,
                icon: Icons.email,
                onTap: () {
                  cantor.enviarEmail();
                },
              ),
              SizedBox(
                height: 25.0,
              ),
              Text(
                cantor.descricao!,
                style: TextStyle(fontSize: 20.0),
                textAlign: TextAlign.justify,
              ),
            ],
          ),
        );
      },
    );
  }
}
