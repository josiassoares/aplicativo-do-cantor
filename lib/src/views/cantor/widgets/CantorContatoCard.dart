import 'package:flutter/material.dart';
import '../../../theme.dart';

class CantorContatoCard extends StatelessWidget {
  const CantorContatoCard({
    Key? key,
    required this.text,
    required this.icon,
    required this.onTap,
  }) : super(key: key);

  final String text;
  final IconData icon;
  final Function onTap;

  @override
  Widget build(BuildContext context) {
    return Card(
      color: backgroundCantorCardColor,
      margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
      child: ListTile(
        onTap: () {
          onTap();
        },
        leading: Icon(
          icon,
          color: iconCantorCardColor,
        ),
        title: Text(
          text,
          style: TextStyle(
            color: Colors.white,
            fontFamily: 'Source Sans Pro',
            fontWeight: FontWeight.w300,
            fontSize: 18.0,
          ),
        ),
      ),
    );
  }
}
