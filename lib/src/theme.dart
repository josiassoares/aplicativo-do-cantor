import 'package:flutter/material.dart';

Color iconColor = Color.fromRGBO(0, 150, 136, 1);
Color backgroundCantorCardColor = Color.fromRGBO(0, 150, 136, 0.6);
Color iconCantorCardColor = Color.fromRGBO(178, 223, 219, 0.9);
