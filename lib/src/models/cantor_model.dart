import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class CantorModel extends ChangeNotifier {
  String? _nome = "Marcela Rocha";
  String? _telefone = "(33) 99916 0843";
  String? _email = "marcela@gmail.com";
  String? _descricao =
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
  String? _urlImagemPerfil = "assets/images/imagemperfil.jpg";

  String? get nome => _nome;
  String? get telefone => _telefone;
  String? get email => _email;
  String? get descricao => _descricao;
  String? get urlImagemPerfil => _urlImagemPerfil;
  String? get telefoneFormatado =>
      _telefone!.replaceAll(new RegExp(r"\s+\b|\b\s"), "");

  enviarMensagemWhatsapp() async {
    var whatsappUrl =
        "whatsapp://send?phone=+55$telefoneFormatado&text=Olá,tudo bem?";

    if (await canLaunch(whatsappUrl)) {
      await launch(whatsappUrl);
    } else {
      throw 'Não foi possível acessar $whatsappUrl';
    }
  }

  enviarEmail() async {
    final Uri params = Uri(
      scheme: 'mailto',
      path: _email,
    );

    String url = params.toString();
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      print('Não foi possível acessar $url');
    }
  }

  fazerLigacao() async {
    var url = "tel:$telefoneFormatado";

    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Não foi possível acessar $url';
    }
  }
}
