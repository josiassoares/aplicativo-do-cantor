import 'package:app_cantor/src/models/cantor_model.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group('CantorModel', () {
    test('Retorna o nome do cantor', () {
      expect(CantorModel().nome, "Marcela Rocha");
    });

    test('Retorna o número de telefone do cantor', () {
      expect(CantorModel().telefone, "+55 39 4125 5414");
    });

    test('Retorna o email do cantor', () {
      expect(CantorModel().email, "marcela@gmail.com");
    });

    test('Retorna a descrição do cantor', () {
      expect(CantorModel().descricao,
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.");
    });

    test('Retorna a url da imagem de perfil do cantor', () {
      expect(CantorModel().urlImagemPerfil, "assets/images/imagemperfil.jpg");
    });
  });
}
